import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Link from '@material-ui/core/Link';
import makeStyles from '@material-ui/core/styles/makeStyles';
import LocalCafeIcon from '@material-ui/icons/LocalCafe';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';


const useStyles = makeStyles((theme) => ({
    card: {
        background: '#FFF6E5',
    },
    table: {
        tableLayout: 'fixed',
        marginTop: '20px',
    },
    iconCol: {
        padding: '0px',
        paddingTop: '5px',
        width: '40px',
        borderBottom: 'none',
        color: theme.palette.primary.light,
    },
    textCol: {
        padding: '0px',
        borderBottom: 'none',
        color: theme.palette.primary.light,
    }

}));


export default function CafeCard(props) {
    const classes = useStyles();
    const {cafe} = props;

    return (
        <Card className={classes.card}>
            {/* Image */}
            <CardMedia component="img" src={cafe.imageLink} title={cafe.imageDescription} alt={cafe.imageDescription}/>

            <CardContent>
                {/*Cafe name*/}
                <Typography color="primary" variant="h3">{cafe.name}</Typography>

                {/*Address and phone*/}
                <Table className={classes.table}>
                    <TableBody>
                        {/* Address */}
                        <TableRow>
                            <TableCell className={classes.iconCol}>
                                <LocalCafeIcon />
                            </TableCell>
                            <TableCell className={classes.textCol}>
                                <Typography variant="body1">
                                    {cafe.street}, {cafe.city}
                                    <br />
                                    {cafe.zipCode}, {cafe.stateWithCountry}
                                </Typography>
                            </TableCell>
                        </TableRow>
                        {/* Phone */}
                        <TableRow>
                            <TableCell className={classes.iconCol} >
                                <PhoneIcon/>
                            </TableCell>
                            <TableCell className={classes.textCol}>
                                <Typography variant="body1">
                                    {cafe.phone}
                                </Typography>
                            </TableCell>
                        </TableRow>
                        {/* Email */}
                        <TableRow>
                            <TableCell className={classes.iconCol} >
                                <EmailIcon/>
                            </TableCell>
                            <TableCell className={classes.textCol}>
                                <Typography variant="body1">
                                    <Link href={`mailto:${cafe.email}`}>{cafe.email}</Link>
                                </Typography>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

            </CardContent>

            {/*Map button*/}
            <CardActions>
                <Button color="primary" onClick={() => props.onOpenMap([cafe.street, cafe.city, cafe.zipCode, cafe.stateWithCountry].join(','))}>
                    See on map
                </Button>
            </CardActions>
        </Card>
    );
}

CafeCard.propTypes = {
    cafe: PropTypes.shape({
        imageLink: PropTypes.string,
        imageDescription: PropTypes.string,
        name: PropTypes.string,
        email: PropTypes.string,
        street: PropTypes.string,
        city: PropTypes.string,
        zipCode: PropTypes.string,
        stateWithCountry: PropTypes.string,
        phone: PropTypes.string,
    })
};

