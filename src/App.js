import React from 'react';
import { makeStyles, createMuiTheme, responsiveFontSizes, ThemeProvider } from '@material-ui/core/styles';
import { Spinner } from '@simply007org/react-spinners';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import GoogleMap from "./Components/GoogleMap";
import CafeList from './Pages/CafeList';
import ApplicationBar from "./Components/ApplicationBar";
import coffeebeanBG from './Media/coffeebeans.png';
import Footer from './Components/Footer';

let theme = createMuiTheme({
    typography: {
        fontFamily: [
            '"Big Shoulders Text"',
            'Roboto',
            '"Segoe UI"',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
        ].join(','),
        h2: {
            fontWeight: 'bold',
            fontSize: '48px',
        }
    },
    palette: {
        primary: {
            main: '#502E2D',
        },
        secondary: {
            main: '#389C5B',
        },
    }
});

theme = responsiveFontSizes(theme);

const useStyles = makeStyles((theme) => ({
    '@global': {
        html: {
            backgroundImage: `url(${coffeebeanBG})`,
            backgroundRepeat: 'repeat',
        },
    },
    root: {
        flexGrow: 1,
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    loaderBG: {
        position: 'fixed',
        zIndex: 10,
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'rgba(226, 226, 226, 0.5)',
    },
    loader: {
        border: 'solid 15px transparent',
        borderBottomColor: '#1d1d1d',
        borderRadius: '50%',
        content: '',
        height: '10em',
        width: '10em',
        animation: 'spin 1s infinite linear',
    },
    content: {
        flexGrow: '1',
        marginBottom: '50px',
        minHeight: '500px',
    },
}));

export default function ButtonAppBar() {
    const classes = useStyles();
    const [mapAddress, setMapAddress] = React.useState(null);

    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <Spinner name="apiSpinner">
                    <div className={classes.loaderBG}>
                        <div className={classes.loader} />
                    </div>
                </Spinner>

                {/*Map Drawer*/}
                <SwipeableDrawer anchor="bottom" open={!!mapAddress} onClose={() => setMapAddress(null)} transitionDuration={400} onOpen={()=>{}}>
                    <GoogleMap address={mapAddress}/>
                </SwipeableDrawer>

                {/*Top Menu bar*/}
                <ApplicationBar />

                {/*Main content with cafe cards*/}
                <div className={classes.content}>
                    <CafeList onOpenMap={(address) => setMapAddress(address)} />
                </div>

                {/*Footer content with contact and social buttons, etc */}
                <Footer />
            </div>
        </ThemeProvider>
    );
}
