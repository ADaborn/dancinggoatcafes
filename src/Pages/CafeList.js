import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { spinnerService } from '@simply007org/react-spinners/';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { CafeStore } from '../Stores/CafeStore';
import CafeCard from "../Components/CafeCard";


const useStyles = makeStyles(() => ({
  cardGrid: {
    marginLeft: '0px',
    marginRight: '0px',
    width: '100%',
  },
  '@media (min-width: 600px)': {
    cardGrid: {
      marginLeft: '20px',
      marginRight: '20px',
      width: 'calc(100% - 40px)',
    },
  },
  '@media (min-width: 1920px)': {
    cardGrid: {
      marginLeft: 'auto',
      marginRight: 'auto',
      maxWidth: '1880px',
    },
  },

}));

export default function CafeList(props) {
  const classes = useStyles();

  const[cafes, changeCafes] = useState([]);

  useEffect(() => {
    CafeStore.addChangeListener(onChange);
    CafeStore.provideCompanyCafes();
    if (spinnerService.isShowing('apiSpinner') === false) {
      spinnerService.show('apiSpinner');
    }

    return function cleanup() {
      spinnerService.hide('apiSpinner');
      CafeStore.removeChangeListener(onChange);
      CafeStore.unsubscribe();
    }
  }, []);


  const onChange = () => {
    spinnerService.hide('apiSpinner');
    changeCafes(CafeStore.getCompanyCafes());
  }

  let createModel = cafe => {
    let model = {
      name: cafe.system.name,
      email: cafe.email.value,
      imageLink: cafe.photo.value[0].url,
      imageDescription: cafe.photo.value[0].description,
      street: cafe.street.value,
      city: cafe.city.value,
      zipCode: cafe.zipCode.value,
      country: cafe.country.value,
      state: cafe.state.value,
      phone: cafe.phone.value,
      id: cafe.system.id,
    };
    model.dataAddress = model.city + ', ' + model.street;
    model.stateWithCountry = (model.state? model.state + ', ' : '') + model.country;
    model.location = model.city + ', ' + model.stateWithCountry;

    return model;
  };

  let companyCafes = cafes
    .map(createModel)
    .map((model) => {
      return (
          <Grid item xl={2} lg={3} md={4} sm={6} xs={12} key={model.id}>
            <CafeCard cafe={model} onOpenMap={props.onOpenMap} />
          </Grid>
      );
    });


    return (
      <>
        <Box display="flex" alignItems="flex-start" height={100} justifyContent="center">
          <Typography color="primary" variant="h2">
            OUR CAFES
          </Typography>
        </Box>
        <Grid container spacing={3} className={classes.cardGrid}>
          {companyCafes}
        </Grid>
      </>
    );
}

CafeList.propTypes = {
  onOpenMap: PropTypes.func,
};
