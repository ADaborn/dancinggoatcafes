# Dancing Goats Demo code

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Building and running the demo locally
Download, or use git clone.

Download dependencies by navigating to the dancinggoatdemo folder in a terminal and running `yarn`

Once that is finished, start the dev server locally using `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
